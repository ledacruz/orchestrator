package com.mba.orchestrator.controller;

import com.google.gson.Gson;
import com.mba.orchestrator.messaging.OrchestratorProducer;
import com.mba.orchestrator.model.Orchestrator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

@RestController
    @RequestMapping(value = "/kafka")
    public class OrchestratorController {

        private final OrchestratorProducer orchestratorProducer;

        @Autowired
        public OrchestratorController(OrchestratorProducer orchestratorProducer) {
            this.orchestratorProducer = orchestratorProducer;
        }
        @PostMapping(value = "/publish/cho/orchestrator")
        public void sendMessageToKafkaTopic(@RequestParam("message") String message){
            this.orchestratorProducer.sendMessage(convertOrchestratorToString(message), "choreography-topic");
        }

        private String convertOrchestratorToString(String message){
            Gson gson = new Gson();
            Orchestrator orchestrator = Orchestrator.builder().eventType(message).timestamp(getDateTime()).build();;
            String finalMessage = gson.toJson(orchestrator);
            return finalMessage;
        }

    private String getDateTime() {
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy-HH:mm:ss.SSS");
        Date date = new Date();
        return dateFormat.format(date);
    }
    }
