package com.mba.orchestrator.model;


import lombok.*;

@Data
@Builder
public class Orchestrator {

    public String eventType;
    public String timestamp;
}
