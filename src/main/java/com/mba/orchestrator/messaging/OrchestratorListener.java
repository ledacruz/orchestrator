package com.mba.orchestrator.messaging;

import com.google.gson.Gson;
import com.mba.orchestrator.model.Orchestrator;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class OrchestratorListener {

    @Autowired
    OrchestratorProducer orchestratorProducer;

    @Value("${topic-out}")
    private String orchestrationTopic;

    private final Logger logger = LoggerFactory.getLogger(OrchestratorListener.class);

    private String producerMessage;

    @KafkaListener(topics = "${topic-in}", groupId = "group-orchestrator")
    public void consumeOrchestration(String message){
        logger.info(String.format("$$ -> Consumed Message -> %s",message));

        Orchestrator orchestrator = convertStringtoOrchestrator(message);

        switch (orchestrator.getEventType()) {
            case "transfer.solicited":
                producerMessage = "accounts.requested";
                callProducer(producerMessage, orchestrationTopic);
                break;
            case "accounts.sucess":
                producerMessage = "receipts.requested";
                //producerMessage = "orchestrator.undo.failure";
                callProducer(producerMessage, orchestrationTopic);
                break;
            case "receipts.sucess":
                producerMessage = "fraud.requested";
                //producerMessage = "orchestrator.undo.failure";
                callProducer(producerMessage, orchestrationTopic);
                break;
            case "fraud.sucess":
                producerMessage = "transfer.sender.requested";
                callProducer(producerMessage, orchestrationTopic);
                break;
            case "transferSender.sucess":
                producerMessage = "balance.requested";
                callProducer(producerMessage, orchestrationTopic);
                producerMessage = "tax.requested";
                callProducer(producerMessage, orchestrationTopic);
                break;
            case "tax.sucess":
            case "balance.sucess" :
                producerMessage = "transfer-finished!!!";
                callProducer(producerMessage, orchestrationTopic);
                break;
            case "accounts.failure":
                producerMessage = "fail.doing.accounts";
                callProducer(producerMessage, orchestrationTopic);
                break;
            case "receipts.failure":
            case "fraud.failure":
            case "transferSender.failure":
                producerMessage = "undo.requested";
                callProducer(producerMessage, orchestrationTopic);
                break;

            case "tax.failure":
            case "balance.failure":
                producerMessage = "tax/balance.fail. Suport look up";
                callProducer(producerMessage, orchestrationTopic);
                break;
            case "accounts.undo.sucess":
            case "receipts.undo.sucess":
            case "fraud.undo.sucess":
            case "transfer.sender.undo.sucess":
                producerMessage = "Undo realized with sucess";
                callProducer(producerMessage, orchestrationTopic);
                break;
            case "accounts.undo.failure":
            case "receipts.undo.failure":
            case "fraud.undo.failure":
            case "transfer.sender.undo.failure":
                producerMessage = "Undo realized with fail";
                callProducer(producerMessage, orchestrationTopic);
                break;
            default:
                logger.info(String.format("$$ -> Consumed not a know Message -> %s",message));
        }
    }

    private void callProducer(String message, String topic){

        orchestratorProducer.sendMessage(producerMessage, topic);
    }

    private Orchestrator convertStringtoOrchestrator(String message){
        Gson gson = new Gson();
        Orchestrator orchestrator = gson.fromJson(message, Orchestrator.class);

        return orchestrator;

    }


}
